'use strict';

var _ = require('underscore');
var LoopBackContext = require('loopback-context');

module.exports = function (Model, options) {
    Model.dataSource.defineRelations(Model, {
        empresa: {
            type: 'belongsTo',
            model: 'Empresa',
            foreignKey: 'emp_id',
            options: {
                required: true
            }
        }
    });

    Model.observe('before save', function (ctx, next) {
        var _ctx = LoopBackContext.getCurrentContext();
        var currentUser = _ctx && _ctx.get('currentUser');
        try{
            var empresa = currentUser.emp_id;
        }
        catch(e){

        }

        if (ctx.instance) {
            // debug('%s.%s before save: %s', ctx.Model.modelName, "empresa", ctx.instance.id);
            ctx.instance["emp_id"] = empresa;
        } else {
            // debug('%s.%s before update matching %j', ctx.Model.pluralModelName, "empresa", ctx.where);
            ctx.data["emp_id"] = empresa;
        }
        return next();
    });
    // Model.createOptionsFromRemotingContext = function (ctx) {
    //     var base = this.base.createOptionsFromRemotingContext(ctx)
    //     base.ctx = ctx.req
    //     return base
    // }
    // Model.observe('access', function (ctx, next) {
    //     const token = ctx.options && ctx.options.accessToken;
    //     const userId = token && token.userId;
    //     next();
    // });

    /**
     * Find enterprise
     *
     * @param   {object}    where    Where Filter
     * @param   {function}  cb       Async Callback
     */
    const _find = Model.find
    Model.find = function () {
        var filter = arguments[0];
        var options = arguments[1];
        var cb = arguments[2];

        if (filter === undefined) filter = {}
        else {
            if (!filter.where) filter.where = {}
            var _ctx = LoopBackContext.getCurrentContext();
            var _test = LoopBackContext.perRequest();
            var currentUser = _ctx && _ctx.get('currentUser');
            try{
                var empresa = currentUser.emp_id;
                if(empresa !== 0){
                if (filter.where.and !== undefined) {
                    filter.where.and.push({'emp_id': empresa});
                }else{
                    filter.where = {
                        and: [filter.where, {
                            'emp_id': empresa
                        }]
                    };
                }
            }
            }
            catch(e){

            }

            // Empresa 0 >>> View all
            
        }
        return _find.call(Model, filter, options, cb)
    };
};
